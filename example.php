<?php
require __DIR__ . '/vendor/autoload.php';
ini_set('display_errors', 1);

use MichalJozwiak\Cesar;
use MichalJozwiak\Atbash;
use MichalJozwiak\Bacon;


//** Usage example Cesar Cipher */
$cipher = new Cesar('ananas', 1);
echo $cipher->encrypt();

echo '<br>';

$cipher = new Cesar('bobobt', 1);
echo $cipher->decrypt();

echo '<br>';
echo '<br>';

//** Usage example AtBash Cipher */
$cipher = new Atbash('ananas');
echo $cipher->encrypt();

echo '<br>';

$cipher = new Atbash('zmzmzh');
echo $cipher->decrypt();


echo '<br>';
echo '<br>';

//** Usage example Bacon Cipher */
$cipher = new Bacon('ananas', 26);
echo strtoupper($cipher->encrypt());

echo '<br>';

$cipher = new Bacon('aaaaaabbabaaaaaabbabaaaaabaaba', 26);
echo $cipher->decrypt();

