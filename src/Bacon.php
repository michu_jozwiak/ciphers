<?php
/**
 * Created by Michal Jozwiak
 * Project: michaljozwiak/ciphers
 * Email: <michu.jozwiak@gmail.com>
 */

namespace MichalJozwiak;

use MichalJozwiak\Cipher\Cipher;
use MichalJozwiak\Cipher\CipherInterface;

/**
 * Class Bacon
 * @package MichalJozwiak
 */
class Bacon
    extends Cipher implements CipherInterface
{
    const AVAILABLE_TYPES_OF_ALPHABET = [24, 26];

    /**
     * @var
     */
    protected $_typeOfAlphabet;

    /**
     * @return mixed
     */
    public function getTypeOfAlphabet() : int
    {
        return $this->_typeOfAlphabet;
    }

    /**
     * @param $typeOfAlphabet
     * @throws \Exception
     */
    public function setTypeOfAlphabet(int $typeOfAlphabet)
    {
        if (!in_array($typeOfAlphabet, self::AVAILABLE_TYPES_OF_ALPHABET)){
            throw new \Exception('Wrong alphabet type.');
        }

        $this->_typeOfAlphabet = $typeOfAlphabet;
    }

    /**
     * @return $this
     */
    public function setAlphabet()
    {
        $alphabet = range(parent::RANG_START, parent::RANG_END);

        $baconAlphabet = [];
        $letterCounter = 0;
        foreach ($alphabet as $letter) {
            $binaryLetter = sprintf("%05d", decbin($letterCounter));
            $baconLetter = str_replace(['0', '1'], ['a', 'b'], $binaryLetter);

            $baconAlphabet[$letter] = $baconLetter;

            if (('i' === $letter  || 'u' === $letter) && 24 === $this->getTypeOfAlphabet()) {
                continue;
            }
            $letterCounter++;
        }
        $this->_alphabet = $baconAlphabet;

        return $this;
    }

    /**
     * Bacon constructor.
     * @param string $text
     * @param int $typeOfAlphabet
     */
    public function __construct(string $text, int $typeOfAlphabet)
    {
        $this->setTypeOfAlphabet($typeOfAlphabet);

        parent::__construct($text);
    }

    /**
     * @return string
     */
    public function encrypt() : string
    {
        return $this->changeLetters($this->getAlphabet(), 1);
    }

    /**
     * @return string
     */
    public function decrypt() : string
    {
        return $this->changeLetters(array_flip($this->getAlphabet()), 5);
    }
}