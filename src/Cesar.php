<?php
/**
 * Created by Michal Jozwiak
 * Project: michaljozwiak/ciphers
 * Email: <michu.jozwiak@gmail.com>
 */

namespace MichalJozwiak;

use MichalJozwiak\Cipher\Cipher;
use MichalJozwiak\Cipher\CipherInterface;

/**
 * Class Cesar
 * @package MichalJozwiak
 */
class Cesar
    extends Cipher implements CipherInterface
{
    /**
     * @var int
     */
    protected $_shifts;

    /**
     * @return int|null
     */
    public function getShifts() : int
    {
        return $this->_shifts;
    }

    /**
     * @param $shifts
     * @return $this
     */
    public function setShifts(int $shifts)
    {
        $this->_shifts = $shifts;

        return $this;
    }

    /**
     * @return $this
     */
    public function setAlphabet()
    {
        $asciiRange = range(parent::RANG_START, parent::RANG_END);

        foreach ($asciiRange as $letter) {
            $this->_alphabet[$letter] =
                chr(ord(parent::RANG_START)+(ord($letter)-(ord(parent::RANG_START)-$this->getShifts())) % 26 );
        }

        return $this;
    }

    /**
     * Cesar constructor.
     * @param string $text
     * @param int $shifts
     */
    public function __construct(string $text, int $shifts)
    {
        $this->setShifts($shifts);

        parent::__construct($text);
    }

    /**
     * @return string
     */
    public function encrypt() : string
    {
        return $this->changeLetters($this->getAlphabet());
    }

    /**
     * @return string
     */
    public function decrypt() : string
    {
        return $this->changeLetters(array_flip($this->getAlphabet()));
    }
}