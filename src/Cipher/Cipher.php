<?php
/**
 * Created by Michal Jozwiak
 * Project: michaljozwiak/ciphers
 * Email: <michu.jozwiak@gmail.com>
 */

namespace MichalJozwiak\Cipher;

/**
 * Class Cipher
 * @package MichalJozwiak\Cipher
 */
class Cipher
{
	const RANG_START = 'a';

	const RANG_END = 'z';

	/**
	 * @var string
     */
	protected $_text;

	/**
	 * @var
     */
	protected $_alphabet;

	/**
	 * @return string
	 */
	public function getText() : string
	{
		return $this->_text;
	}

	/**
	 * @param string $text
	 * @return $this
	 * @throws \Exception
     */
	public function setText(string $text)
	{
		if (preg_match('~[0-9]+~', $text)) {
			throw new \Exception('String contain numbers. Operation Forbidden!');
		}
		$this->_text = strtolower($text);

		return $this;
	}

	/**
	 * @return array
     */
	public function getAlphabet() : array
	{
		return $this->_alphabet;
	}

	/**
	 * @return $this
     */
	public function setAlphabet()
	{
		$this->_alphabet = [];

		return $this;
	}

	/**
	 * Cipher constructor.
	 * @param string $text
     */
	public function __construct(string $text)
	{
		$this->setText($text);
		$this->setAlphabet();
	}

	/**
	 * @param array $alphabet
	 * @param int $letterSize
	 * @return string
     */
	protected function changeLetters(array $alphabet, int $letterSize = 1) : string
	{
		$wordsArray = explode(' ', $this->getText());

		$changedWords = [];
		foreach ($wordsArray as $word) {

			$wordLetters = str_split($word, $letterSize);
			$changedLetter = [];
			foreach ($wordLetters as $letter) {
				$changedLetter[] = $alphabet[$letter];
			}

			$changedWords[] = implode('', $changedLetter);
		}

		return implode(' ', $changedWords);
	}
}