<?php
/**
 * Created by Michal Jozwiak
 * Project: michaljozwiak/ciphers
 * Email: <michu.jozwiak@gmail.com>
 */

namespace MichalJozwiak\Cipher;

interface CipherInterface
{
    public function encrypt() : string;

    public function decrypt() : string;
}