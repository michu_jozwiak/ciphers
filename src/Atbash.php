<?php
/**
 * Created by Michal Jozwiak
 * Project: michaljozwiak/ciphers
 * Email: <michu.jozwiak@gmail.com>
 */

namespace MichalJozwiak;

use MichalJozwiak\Cipher\Cipher;
use MichalJozwiak\Cipher\CipherInterface;

/**
 * Class Atbash
 * @package MichalJozwiak
 */
class Atbash
    extends Cipher implements CipherInterface
{
    /**
     * @return $this
     */
    public function setAlphabet()
    {
        $alphabet = range(parent::RANG_START, parent::RANG_END);
        $reversedAlphabet = array_reverse($alphabet);

        $this->_alphabet = array_combine($alphabet, $reversedAlphabet);

        return $this;
    }

    /**
     * Atbash constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        parent::__construct($text);
    }

    /**
     * @return string
     */
    public function encrypt() : string
    {
        return $this->changeLetters($this->getAlphabet());
    }

    /**
     * @return string
     */
    public function decrypt() : string
    {
        return $this->encrypt();
    }
}